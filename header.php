<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Program do zarządzania kolejkami</title>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<link href="style.css" rel="stylesheet">
</head>
  <body>
      
<div class="container">
<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link" href="index.php">INDEX</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="customer.php">KLIENT</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="printTicket.php">PRINT TICKET</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="1.php">page 1</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="2.php">page 2</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="">Room no: <?php echo $_SESSION['roomNumberSession'] ?></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="">Ticket no: <?php echo $_SESSION['ticketNumberSession'] ?></a>
    </li>
  </ul>

</nav>
</div>