<?php 
global $largestNumber;

function fetch_data(){
    $output = '';
    $connect = mysqli_connect("localhost", "root", "", "queuedb");

    $rowSQL = mysqli_query($connect, "SELECT MAX( id ) AS max FROM wizyta" );
    $row2 = mysqli_fetch_array( $rowSQL );
    $largestNumber = $row2['max'];
    
    $query = "SELECT * FROM wizyta where id = " . $largestNumber;
    $result = mysqli_query($connect, $query);
    while($row = mysqli_fetch_array($result)){
        $output .= 
    '<table width="100%" cellspacing="0" cellpadding="55%">
        <tr height="60px"><td colspan="2" align="center">Nazwa zakladu</td></tr>
        <tr valign="bottom">
            <td align="left" valign="bottom" width="35%" ><span style="font-size:14px">  p. </span><span style="font-size:30px">1.' .$row["pokoj"].'</span></td>
            <td align="right" valign="bottom" width="65%" height="60px"> <span style="font-size:50px">' .$row["numer"].'</span></td>
        </tr>
        <tr height="60px"><td colspan="2" align="right" valign="middle">'.$row["data"].'</td></tr>
        <tr><td colspan="2" align="right" valign="middle">#'.$row["id"].'</td></tr>  
    </table>';  
    }
    return $output;
}

if(isset($_POST["create"])){
    require_once('TCPDF/tcpdf.php');
    $width = 80;
    $height = 50;
    $pdf = new TCPDF('L', 'mm', array($width, $height), true, 'UTF-8', false);
    $pdf->SetPrintHeader(false);
    $pdf->SetPrintFooter(false);
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetMargins(2,3, 2, true);
    $pdf->AddPage();
    $pdf->SetAutoPageBreak(True, 1);
                                       
    //$pdf->writeHTMLCell(0, 0, '', '', $html.fetch_data(), 0, 1, 0, true, '', true);
    $pdf->writeHTMLCell(0, 0, '', '', fetch_data(), 0, 0, 0, false, '', false);
    //$pdf->Output('ticket.pdf', 'F');
    $pdf->Output('aa.pdf', 'I');
    //$pdf->Output('001.pdf', 'I');
    }
?>

<?php include "topLinks.php" ?>

<html>
    <body>
    <div class="container">
        <h2>Print last created ticket</h2>
        <form method="post">
            <input type="submit" name="create" class="btn btn-danger" value="Create" /> 
        </form>      
    </div>
    </body>
</html>